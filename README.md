Tehran Traffic Map
==================

<b>Tehran and Roads Traffic Maps Android Application</b><br />
Designed By: <a href="http://www.mirhoseini.com" target="_blank"><b>Mohsen Mirhoseini Argi</b></a>

This software use your mobile internet connection (WIFI/GPRS) to show a real-time Traffic information for surface street traffic conditions within the city of Tehran. All information received from "Tehran Traffic Control Co." cameras server and visualize on a map. It is a dual language program (Persian/English depending on device default language).
It also lets you zoom on the streets and compare new received and older map.

Published at: <a href="https://play.google.com/store/apps/details?id=com.tehran.traffic"> Google Play</a> | <a href="http://cafebazaar.ir/app/com.tehran.traffic" target="_blank">Cafe Bazaar</a> | <a href="http://cando.asr24.com/app.jsp?appId=291953" target="_blank">Cando Market</a>
Telegram Bot: <a href="https://telegram.me/TehranTrafficMapBot" target="_blank">https://telegram.me/TehranTrafficMapBot</a>
Facebook Fan Page: <a href="http://www.facebook.com/tehrantrafficmap" target="_blank">http://www.facebook.com/tehrantrafficmap</a>
Contact me: mohsen@mirhoseini.com
